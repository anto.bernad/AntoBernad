//Including Modules
const express = require('express');
const bodyParser = require('body-parser');
const url = require('url');
// create express app
const app = express();
app.use(bodyParser.json())

// API sum up two numbers
app.get('/calcs', function (req, res) {
   res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');
   total_sum=parseInt(req.param('first_num'))+parseInt(req.param('second_num'));
   // total_sum=parseInt(req.params.first_num)+parseInt(req.params.second_num);

   res.json(total_sum);
});
app.listen(3000, () => {
    console.log("Server is listening on port 3000");
});
