$(document).ready(function(){
	$("#button-addon1").on("click",function(){
		// $("#results").remove()
		var expr=(encodeURIComponent($("#expr").val()))
		$.ajax({
		async:'true',
		type:'GET',
		url:'http://api.mathjs.org/v4/?expr='+expr,
		statusCode: {
        200: function (data) {
      console.log("200 - Success");
			$("#results").text(data);
			$("#error").empty()
        },
        400: function(request, status, error) {
            console.log("400 - Not Found");
            // console.log(error);
            $("#results").empty()
            $("#error").append('<div class="w-100 alert alert-danger" role="alert">'+"Invalid expression"+'</div>')
        }
    }
	});
	});
});
