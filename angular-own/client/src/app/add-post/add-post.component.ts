import { Component } from '@angular/core';
import { AddPostService } from './add-post.service';
import { Post } from '../models/post.model';
 
@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.css'],
  providers: [ AddPostService ]
})
export class AddPostComponent {
 
  public post : Post;
 
  constructor(private addPostService: AddPostService) {
      this.post = new Post();
  }
 
  addPost() {
    if(this.post.title && this.post.description){
        this.addPostService.addPost(this.post).subscribe(res =>{
            console.log('response is ', res)
        });
    } else {
        alert('Title and Description required');
    }
  }
 
}